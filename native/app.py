# Import dependencies
from flask import Flask, g
from flask_oidc import OpenIDConnect

# Create a Flask app and set the OpenID connect config params
app = Flask(__name__)
app.secret_key = 'vrS8Tc5IMVeOHQSRSJWXgjJ61KWKWqKw'
app.config['OIDC_CLIENT_SECRETS'] = 'oidc-config.json'
app.config['OIDC_COOKIE_SECURE'] = False

# Instantiate a flask_oidc object that will be used to handle the authentication flow
oidc = OpenIDConnect(app)

# Function decorators to set the app route and the required login
@app.route('/')
@oidc.require_login
def index():
    # Check if user is authenticated
    if oidc.user_loggedin:
        # If user is authenticated, run this code
        return 'Welcome %s' % oidc.user_getfield('preferred_username')
    else:
        # If he's not, run this code
        return 'Not logged in'
